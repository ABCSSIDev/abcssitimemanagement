package com.resocoder.materialdesign2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;

import com.daimajia.androidanimations.library.Techniques;
import com.resocoder.materialdesign2.app_config.AppConfig;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

public class SplashActivity extends AwesomeSplash {

    @Override
    public void initSplash(ConfigSplash cs1) {
        getAndSetSplashValues(cs1);
    }

    @Override
    public void animationsFinished() {
        //wait 5 sec and then go back to MainActivity
        final Activity a = this;
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(a,MainActivity.class);
                startActivity(intent);
            }
        }, AppConfig.SPLASH_DELAY);
    }


    public void getAndSetSplashValues(ConfigSplash cs1) {

            cs1.setAnimCircularRevealDuration(1000);
            cs1.setRevealFlagX(Flags.REVEAL_RIGHT);
            cs1.setRevealFlagY(Flags.REVEAL_BOTTOM);
            cs1.setBackgroundColor(R.color.colorSplash);
            cs1.setLogoSplash(R.drawable.ic_pgc3);
            cs1.setAnimLogoSplashTechnique(Techniques.BounceIn);
            cs1.setAnimLogoSplashDuration(1000);

//            cs1.setPathSplash(AppConfig.DROID_LOGO);
//            cs1.setPathSplashStrokeSize(3);
//            cs1.setPathSplashStrokeColor(R.color.colorAccent);
//            cs1.setPathSplashFillColor(R.color.fillColor);
//            cs1.setOriginalHeight(400);
//            cs1.setOriginalWidth(400);
//            cs1.setAnimPathStrokeDrawingDuration(1500);
//            cs1.setAnimPathFillingDuration(1500);
//
          cs1.setTitleSplash("Pogoy Group of Companies");
            cs1.setTitleTextColor(R.color.white);
            cs1.setTitleTextSize(30f); //float value
          cs1.setAnimTitleDuration(1500);
           cs1.setAnimTitleTechnique(Techniques.FadeIn);
           cs1.setTitleFont("fonts/volatire.ttf");
    }
}
