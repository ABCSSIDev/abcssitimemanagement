package com.resocoder.materialdesign2.app_config;

public class AppConfig {
    public static String domain = "http://demosite.infini-ts.com/ITSv2/androidSync";
    public String domain2 = "http://192.168.2.52/android_login_api";
    public static String domain3 = "http://104.238.117.151/android_api";
    public static String domain4 = "http://45.40.136.50/Marvel/public/api/api";
    // Server user login url
    public static String URL_LOGIN = domain3 + "/login.php";

    // Server user register url
    public static String URL_REGISTER = domain + "/register.php";
    public static String ServerURL = domain3 + "/fetchData.php";
    //    public static String ServerURL = domain4;
    // Server user info
    public static String URL_info = domain3 + "/employeeInfo.php";

    public static final String URL_SAVE_NAME = domain3 + "/saveName.php";

    public static final String URL_CHECK_VERSION = domain3 + "/checkVersion.php";

    public static final String URL_APK_DOWNLOAD =  "http://demosite.infini-ts.com/ITSv2/android/apk/time_management.apk";

    public static final String URL_SEND_PROBLEMS  = domain + "/sendProblems.php";

    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;

    public static final int SPLASH_DELAY = 5000;
    public static final String EMPTY = "";

    //fonts
    public static final String STREATWEAR_FONT = "fonts/streatwear.otf";
    public static final String DITI_SWEET_FONT = "fonts/diti_sweet.ttf";
    public static final String GERMANIA_ONE_FONT = "fonts/germania_one.ttf";
    public static final String SLIMJIM_FONT = "fonts/slimjim.ttf";
    public static final String VOLATIRE_FONT = "fonts/volatire.ttf";

    public static final String[] FONTS = {EMPTY, STREATWEAR_FONT, DITI_SWEET_FONT, GERMANIA_ONE_FONT, SLIMJIM_FONT, VOLATIRE_FONT};

    //flags
    public static final String FOR_WHAT = "forWhat";
    public static final int FOR_LOGO = 0;
    public static final int FOR_TEXT = 1;
    public static final String CONFIG = "config";

    //path
    public static final String DROID_LOGO = "M 149.22,22.00\n" +
            "           C 148.23,20.07 146.01,16.51 146.73,14.32\n" +
            "             148.08,10.21 152.36,14.11 153.65,16.06\n" +
            "             153.65,16.06 165.00,37.00 165.00,37.00\n" +
            "             194.29,27.24 210.71,27.24 240.00,37.00\n" +
            "             240.00,37.00 251.35,16.06 251.35,16.06\n" +
            "             252.64,14.11 256.92,10.21 258.27,14.32\n" +
            "             258.99,16.51 256.77,20.08 255.78,22.00\n" +
            "             252.53,28.28 248.44,34.36 246.00,41.00\n" +
            "             252.78,43.16 258.78,48.09 263.96,52.85\n" +
            "             281.36,68.83 289.00,86.62 289.00,110.00\n" +
            "             289.00,110.00 115.00,110.00 115.00,110.00\n" +
            "             115.00,110.00 117.66,91.00 117.66,91.00\n" +
            "             120.91,76.60 130.30,62.72 141.04,52.85\n" +
            "             146.22,48.09 152.22,43.16 159.00,41.00\n" +
            "             159.00,41.00 149.22,22.00 149.22,22.00 Z\n" +
            "           M 70.80,56.00\n" +
            "           C 70.80,56.00 97.60,100.00 97.60,100.00\n" +
            "             101.34,106.21 108.32,116.34 110.21,123.00\n" +
            "             113.76,135.52 103.90,147.92 91.00,147.92\n" +
            "             78.74,147.92 74.44,139.06 69.00,130.00\n" +
            "             69.00,130.00 39.80,82.00 39.80,82.00\n" +
            "             35.73,75.29 28.40,66.08 29.20,58.00\n" +
            "             30.26,47.20 38.61,40.47 49.00,39.72\n" +
            "             61.22,40.24 64.96,46.28 70.80,56.00 Z\n" +
            "           M 375.80,58.00\n" +
            "           C 376.60,66.08 369.27,75.29 365.20,82.00\n" +
            "             365.20,82.00 336.00,130.00 336.00,130.00\n" +
            "             330.71,138.82 326.73,147.24 315.00,147.89\n" +
            "             301.74,148.63 291.14,135.87 294.79,123.00\n" +
            "             296.68,116.34 303.66,106.21 307.40,100.00\n" +
            "             307.40,100.00 333.00,58.00 333.00,58.00\n" +
            "             339.02,47.98 342.23,40.92 355.00,39.72\n" +
            "             365.83,40.00 374.69,46.77 375.80,58.00 Z\n" +
            "           M 289.00,116.00\n" +
            "           C 289.00,116.00 289.00,239.00 289.00,239.00\n" +
            "             288.98,249.72 285.92,257.31 275.00,261.10\n" +
            "             265.22,264.50 258.37,259.56 255.02,264.43\n" +
            "             253.78,266.24 254.00,269.84 254.00,272.00\n" +
            "             254.00,272.00 254.00,298.00 254.00,298.00\n" +
            "             254.00,304.85 254.77,310.07 250.36,315.93\n" +
            "             242.35,326.68 226.84,326.49 218.80,315.93\n" +
            "             215.07,311.00 215.01,306.83 215.00,301.00\n" +
            "             215.00,301.00 215.00,262.00 215.00,262.00\n" +
            "             215.00,262.00 190.00,262.00 190.00,262.00\n" +
            "             190.00,262.00 190.00,301.00 190.00,301.00\n" +
            "             189.99,306.83 189.93,311.00 186.20,315.93\n" +
            "             178.16,326.49 162.65,326.68 154.64,315.93\n" +
            "             151.09,311.22 151.01,307.61 151.00,302.00\n" +
            "             151.00,302.00 151.00,272.00 151.00,272.00\n" +
            "             151.00,269.84 151.22,266.24 149.98,264.43\n" +
            "             146.53,259.42 138.97,264.76 129.00,260.86\n" +
            "             118.39,256.72 116.02,248.29 116.00,238.00\n" +
            "             116.00,238.00 116.00,116.00 116.00,116.00\n" +
            "             116.00,116.00 289.00,116.00 289.00,116.00 Z";

    public static final String logo = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6eGh0bWw9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIGhlaWdodD0iMjEyLjM5OTk5IgogICB3aWR0aD0iMTg5LjIwMjY4IgogICB4bWw6c3BhY2U9InByZXNlcnZlIgogICB2aWV3Qm94PSIwIDAgMTg5LjIwMjY4IDIxMi4zOTk5OSIKICAgeT0iMHB4IgogICB4PSIwcHgiCiAgIGlkPSJySnlENUNuanoiCiAgIHZlcnNpb249IjEuMSI+PG1ldGFkYXRhCiAgIGlkPSJtZXRhZGF0YTE2Ij48cmRmOlJERj48Y2M6V29yawogICAgICAgcmRmOmFib3V0PSIiPjxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PjxkYzp0eXBlCiAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxkZWZzCiAgIGlkPSJkZWZzMTQiIC8+PHhodG1sOnN0eWxlPkAtd2Via2l0LWtleWZyYW1lcyBTa21KdnFBMnNNX1NrZjVqUjNvR19BbmltYXRpb257MzMuMzMley13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7dHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7fTYwJXstd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7dHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTt9MCV7LXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTt0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTt9MTAwJXstd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7dHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTt9fUBrZXlmcmFtZXMgU2ttSnZxQTJzTV9Ta2Y1alIzb0dfQW5pbWF0aW9uezMzLjMzJXstd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO3RyYW5zZm9ybTogcm90YXRlKDBkZWcpO302MCV7LXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO3RyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7fTAley13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7dHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7fTEwMCV7LXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO3RyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7fX1ALXdlYmtpdC1rZXlmcmFtZXMgQmsta3ZjMG5qel9CSnJ2c1JoaUdfQW5pbWF0aW9uezMwJXstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7fTMzLjMzJXstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMTJweCk7dHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAxMnB4KTt9MzYuNjcley13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO3RyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt9MCV7LXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7dHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO30xMDAley13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO3RyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt9fUBrZXlmcmFtZXMgQmsta3ZjMG5qel9CSnJ2c1JoaUdfQW5pbWF0aW9uezMwJXstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7fTMzLjMzJXstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMTJweCk7dHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAxMnB4KTt9MzYuNjcley13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO3RyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt9MCV7LXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7dHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO30xMDAley13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO3RyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt9fUAtd2Via2l0LWtleWZyYW1lcyBCSmx5d3FDM3N6X0FuaW1hdGlvbns3Ni42NyV7b3BhY2l0eTogMTt9OTAle29wYWNpdHk6IDA7fTAle29wYWNpdHk6IDE7fTEwMCV7b3BhY2l0eTogMDt9fUBrZXlmcmFtZXMgQkpseXdxQzNzel9BbmltYXRpb257NzYuNjcle29wYWNpdHk6IDE7fTkwJXtvcGFjaXR5OiAwO30wJXtvcGFjaXR5OiAxO30xMDAle29wYWNpdHk6IDA7fX1ALXdlYmtpdC1rZXlmcmFtZXMgQkpseXdxQzNzel9CSjVvOUEyc01fQW5pbWF0aW9uezYwJXstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7fTY2LjY3JXstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgLTIwcHgpO3RyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgLTIwcHgpO303MCV7LXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7dHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO303My4zMyV7LXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIC0xMHB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIC0xMHB4KTt9NzYuNjcley13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO3RyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt9NzguODkley13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAtNXB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIC01cHgpO304MS4xMSV7LXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7dHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO304Mi4yMiV7LXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIC0zcHgpO3RyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgLTNweCk7fTgzLjMzJXstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7fTAley13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO3RyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt9MTAwJXstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7fX1Aa2V5ZnJhbWVzIEJKbHl3cUMzc3pfQko1bzlBMnNNX0FuaW1hdGlvbns2MCV7LXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7dHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO302Ni42NyV7LXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIC0yMHB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIC0yMHB4KTt9NzAley13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO3RyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt9NzMuMzMley13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAtMTBweCk7dHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAtMTBweCk7fTc2LjY3JXstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7fTc4Ljg5JXstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgLTVweCk7dHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAtNXB4KTt9ODEuMTEley13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO3RyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt9ODIuMjIley13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAtM3B4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIC0zcHgpO304My4zMyV7LXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7dHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO30wJXstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7fTEwMCV7LXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7dHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAwcHgpO319I3JKeUQ1Q25qeiAqey13ZWJraXQtYW5pbWF0aW9uLWR1cmF0aW9uOiAzczthbmltYXRpb24tZHVyYXRpb246IDNzOy13ZWJraXQtYW5pbWF0aW9uLXRpbWluZy1mdW5jdGlvbjogY3ViaWMtYmV6aWVyKDAsIDAsIDEsIDEpO2FuaW1hdGlvbi10aW1pbmctZnVuY3Rpb246IGN1YmljLWJlemllcigwLCAwLCAxLCAxKTt9I0JrLWt2YzBuanp7ZmlsbDogIzQ0NTg3NTt9I0hKemt2OVJuc017ZmlsbDogIzQ0NTg3NTt9I1NrbUp2cUEyc017ZmlsbDogI0ZGRjBDQzt9I0JKbHl3cUMzc3pfQko1bzlBMnNNey13ZWJraXQtYW5pbWF0aW9uLW5hbWU6IEJKbHl3cUMzc3pfQko1bzlBMnNNX0FuaW1hdGlvbjthbmltYXRpb24tbmFtZTogQkpseXdxQzNzel9CSjVvOUEyc01fQW5pbWF0aW9uOy13ZWJraXQtYW5pbWF0aW9uLXRpbWluZy1mdW5jdGlvbjogY3ViaWMtYmV6aWVyKDAuNDIsIDAsIDEsIDEpO2FuaW1hdGlvbi10aW1pbmctZnVuY3Rpb246IGN1YmljLWJlemllcigwLjQyLCAwLCAxLCAxKTstd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46IDUwJSA1MCU7dHJhbnNmb3JtLW9yaWdpbjogNTAlIDUwJTt0cmFuc2Zvcm0tYm94OiBmaWxsLWJveDstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7fSNCSmx5d3FDM3N6ey13ZWJraXQtYW5pbWF0aW9uLW5hbWU6IEJKbHl3cUMzc3pfQW5pbWF0aW9uO2FuaW1hdGlvbi1uYW1lOiBCSmx5d3FDM3N6X0FuaW1hdGlvbjstd2Via2l0LWFuaW1hdGlvbi10aW1pbmctZnVuY3Rpb246IGN1YmljLWJlemllcigwLCAwLCAwLjU4LCAxKTthbmltYXRpb24tdGltaW5nLWZ1bmN0aW9uOiBjdWJpYy1iZXppZXIoMCwgMCwgMC41OCwgMSk7b3BhY2l0eTogMTt9I0JrLWt2YzBuanpfQkpydnNSaGlHey13ZWJraXQtYW5pbWF0aW9uLW5hbWU6IEJrLWt2YzBuanpfQkpydnNSaGlHX0FuaW1hdGlvbjthbmltYXRpb24tbmFtZTogQmsta3ZjMG5qel9CSnJ2c1JoaUdfQW5pbWF0aW9uOy13ZWJraXQtYW5pbWF0aW9uLXRpbWluZy1mdW5jdGlvbjogY3ViaWMtYmV6aWVyKDAuNDIsIDAsIDAuNTgsIDEpO2FuaW1hdGlvbi10aW1pbmctZnVuY3Rpb246IGN1YmljLWJlemllcigwLjQyLCAwLCAwLjU4LCAxKTstd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46IDUwJSA1MCU7dHJhbnNmb3JtLW9yaWdpbjogNTAlIDUwJTt0cmFuc2Zvcm0tYm94OiBmaWxsLWJveDstd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgMHB4KTt0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDBweCk7fSNTa21KdnFBMnNNX1NrZjVqUjNvR3std2Via2l0LWFuaW1hdGlvbi1uYW1lOiBTa21KdnFBMnNNX1NrZjVqUjNvR19BbmltYXRpb247YW5pbWF0aW9uLW5hbWU6IFNrbUp2cUEyc01fU2tmNWpSM29HX0FuaW1hdGlvbjstd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46IDUwJSA1MCU7dHJhbnNmb3JtLW9yaWdpbjogNTAlIDUwJTt0cmFuc2Zvcm0tYm94OiBmaWxsLWJveDstd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO3RyYW5zZm9ybTogcm90YXRlKDBkZWcpO308L3hodG1sOnN0eWxlPgoKPGcKICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTU1LjMsLTM4LjkpIgogICBkYXRhLWFuaW1hdG9yLXR5cGU9IjAiCiAgIGRhdGEtYW5pbWF0b3ItZ3JvdXA9InRydWUiCiAgIGlkPSJCSmx5d3FDM3N6X0JKNW85QTJzTSI+PGcKICAgICBpZD0iQkpseXdxQzNzeiI+Cgk8ZwogICBkYXRhLWFuaW1hdG9yLXR5cGU9IjAiCiAgIGRhdGEtYW5pbWF0b3ItZ3JvdXA9InRydWUiCiAgIGlkPSJCay1rdmMwbmp6X0JKcnZzUmhpRyI+PHBhdGgKICAgICBkPSJtIDE2OS4zLDM4LjkgaCAtMzguNSBjIC0xLjksMCAtMy4zLDEuNSAtMy4zLDMuMyB2IDUuNiBjIDAsMSAwLjgsMS44IDEuOCwxLjggaCA4LjcgYyAxLjQsMCAyLjYsMS4yIDIuNiwyLjYgViA3OSBoIDE5LjEgViA1Mi4zIGMgMCwtMS40IDEuMiwtMi42IDIuNiwtMi42IGggOC43IGMgMSwwIDEuOCwtMC44IDEuOCwtMS44IHYgLTUuNiBjIC0wLjIsLTEuOCAtMS43LC0zLjQgLTMuNSwtMy40IHoiCiAgICAgaWQ9IkJrLWt2YzBuanoiIC8+PC9nPgoJPHBhdGgKICAgZD0ibSAyMzcuMSwxMTkuOSAzLjksLTEuNyBjIDEuOCwtMC43IDIuNSwtMyAxLjUsLTQuNiBsIC04LC0xMy44IGMgLTEuMSwtMS44IC0zLjMsLTIuMyAtNC45LC0xLjEgbCAtMy4zLDIuNCBjIC0yLjgsLTMuOCAtNS45LC03LjUgLTkuMiwtMTAuOSBsIDIuOSwtMi45IGMgMS40LC0xLjMgMS4zLC0zLjcgLTAuMiwtNC45IGwgLTEyLjUsLTkuOSBjIC0xLjUsLTEuMiAtMy44LC0wLjggLTQuOSwwLjggbCAtMi4xLDMuMyBjIC0xNC42LC05LjIgLTMxLjgsLTE0LjUgLTUwLjQsLTE0LjUgLTUyLjMsMCAtOTQuNiw0Mi40IC05NC42LDk0LjYgMCw1Mi4zIDQyLjQsOTQuNiA5NC42LDk0LjYgNTIuMywwIDk0LjYsLTQyLjQgOTQuNiwtOTQuNiAwLjEsLTEyLjkgLTIuNiwtMjUuNCAtNy40LC0zNi44IHoiCiAgIGlkPSJISnprdjlSbnNNIiAvPgoJPGcKICAgZGF0YS1hbmltYXRvci10eXBlPSIxIgogICBkYXRhLWFuaW1hdG9yLWdyb3VwPSJ0cnVlIgogICBpZD0iU2ttSnZxQTJzTV9Ta2Y1alIzb0ciPjxwYXRoCiAgICAgZD0ibSAxNTAsNzkuMSBjIC00MywwIC03Ny44LDM0LjggLTc3LjgsNzcuOCAwLDQzIDM0LjgsNzcuOCA3Ny44LDc3LjggNDMsMCA3Ny44LC0zNC44IDc3LjgsLTc3LjggMCwtNDMgLTM0LjgsLTc3LjggLTc3LjgsLTc3LjggeiBtIDMuOCw3Ny40IGMgMCwyLjEgLTEuOCwzLjggLTMuOCwzLjggLTIuMSwwIC0zLjgsLTEuOCAtMy44LC0zLjggViA5OSBjIDAsLTIuMSAxLjgsLTMuOCAzLjgsLTMuOCAyLDAgMy44LDEuNyAzLjgsMy44IHoiCiAgICAgaWQ9IlNrbUp2cUEyc00iIC8+PC9nPgo8L2c+PC9nPgo8c2NyaXB0CiAgIGlkPSJzY3JpcHQ5Ij4oZnVuY3Rpb24oKXt2YXIgYT1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjckp5RDVDbmp6JyksYj1hLnF1ZXJ5U2VsZWN0b3JBbGwoJ3N0eWxlJyksYz1mdW5jdGlvbihkKXtiLmZvckVhY2goZnVuY3Rpb24oZil7dmFyIGc9Zi50ZXh0Q29udGVudDtnJmFtcDsmYW1wOyhmLnRleHRDb250ZW50PWcucmVwbGFjZSgvdHJhbnNmb3JtLWJveDpbXjtcclxuXSovZ2ksJ3RyYW5zZm9ybS1ib3g6ICcrZCkpfSl9O2MoJ2luaXRpYWwnKSx3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKGZ1bmN0aW9uKCl7cmV0dXJuIGMoJ2ZpbGwtYm94Jyl9KX0pKCk7PC9zY3JpcHQ+PC9zdmc+";
}
