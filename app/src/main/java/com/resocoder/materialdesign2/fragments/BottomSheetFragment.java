package com.resocoder.materialdesign2.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.resocoder.materialdesign2.R;
import com.resocoder.materialdesign2.helpers.DatabaseHelper;
import com.resocoder.materialdesign2.helpers.RoundedImageView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import androidx.annotation.Nullable;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    private static String RFID,name,dept,pos;
    private DatabaseHelper databaseHelper;
    public static BottomSheetFragment newInstance(String rfid){
        RFID = rfid;
        return new BottomSheetFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState){
        View view = layoutInflater.inflate(R.layout.bottom_sheet_layout,container,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        databaseHelper = new DatabaseHelper(getContext());
        HashMap<String,String> employee = databaseHelper.getEmpDetailsById(RFID);
        name = employee.get("name");
        dept = employee.get("department");
        pos = employee.get("position");
        TextView txtName = view.findViewById(R.id.textView4);
        TextView txtRFID = view.findViewById(R.id.textView5);
        TextView txtDpt = view.findViewById(R.id.txtDepartment);
        TextView txtPos = view.findViewById(R.id.txtPosition);
        ImageView imageEmp = view.findViewById(R.id.imageView);
        txtName.setText(name);
        txtRFID.setText(RFID);
        txtDpt.setText(dept);
        txtPos.setText(pos);

        String byteImg = employee.get("image");
        Bitmap bitmap;
        if(byteImg != null && !byteImg.isEmpty() && !byteImg.equals("null")){
            byte[] image = byteImg.getBytes();
            byte[] imageAsBytes = Base64.decode(image,Base64.DEFAULT);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap = RoundedImageView.getRoundedCroppedBitmap(BitmapFactory.decodeByteArray(imageAsBytes,0,imageAsBytes.length),100);
            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
            Bitmap decoded  = BitmapFactory.decodeStream(new ByteArrayInputStream(stream.toByteArray()));
            imageEmp.setImageBitmap(decoded);

        }else{
            bitmap = RoundedImageView.getRoundedCroppedBitmap(BitmapFactory.decodeResource(getActivity().getResources(),
                    R.mipmap.user),100);
            imageEmp.setImageBitmap(bitmap);

        }
    }
}
