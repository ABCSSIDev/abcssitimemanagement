package com.resocoder.materialdesign2.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.resocoder.materialdesign2.MainActivity;
import com.resocoder.materialdesign2.R;
import com.resocoder.materialdesign2.adapters.EmployeeAdapter;
import com.resocoder.materialdesign2.adapters.EmployeeSearchAdapter;
import com.resocoder.materialdesign2.app_config.AppConfig;
import com.resocoder.materialdesign2.app_config.HttpServicesClass;
import com.resocoder.materialdesign2.constructors.Employees;
import com.resocoder.materialdesign2.helpers.Common;
import com.resocoder.materialdesign2.helpers.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;


public class FragmentEmployees extends Fragment {
    private TextView txtEmpty;
    private ListView employeeList;

    private RecyclerView recEmployees;
    private EmployeeAdapter empAdapter;
    private ProgressBar progressBarSubject;
    private String ServerURL = AppConfig.ServerURL + "?loc="+ MainActivity.loc_id;
    private List<Employees> subjectsList;
    public static ArrayList<Integer> empID;
    public static ArrayList<String> empNames,empRFID,empRid;
    public static ArrayList<byte[]> empImages;
    public View shadowView;
    public ImageView gridLayout;
    private DatabaseHelper db;
    ProgressDialog bar;

    @Override
    public View onCreateView(LayoutInflater layoutInflater , @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        return layoutInflater.inflate(R.layout.fragment_employees,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        Context context;
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Menu 2");

        subjectsList    = new ArrayList<>();
        empRid          = new ArrayList<>();
        empID           = new ArrayList<>();
        empRFID         = new ArrayList<>();
        empNames        = new ArrayList<>();
        empImages       = new ArrayList<>();
        db = new DatabaseHelper(this.getContext());

        recEmployees        = view.findViewById(R.id.recyEmp);
        recEmployees.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext());
        recEmployees.setLayoutManager(linearLayoutManager);
        txtEmpty            = view.findViewById(R.id.empty);
        employeeList        = view.findViewById(R.id.listEmployees);
        progressBarSubject  = view.findViewById(R.id.progressBar);
        shadowView          = view.findViewById(R.id.shadowView);
//        SearchView searchView =  view.findViewById(R.id.searchView);
        gridLayout          = view.findViewById(R.id.syncNow);

//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
////                Toast.makeText(getContext(),newText,Toast.LENGTH_LONG).show();
//                if (newText.length() == 0){
//                    loadNames();
//                    gridLayout.setVisibility(View.VISIBLE);
//                }else{
//                    searchEmp(newText);
//                    gridLayout.setVisibility(View.GONE);
//                }
//                return false;
//
//            }
//        });
        loadNames();

        View.OnClickListener syncEmp = new View.OnClickListener(){

            public void onClick(View v) {
//                Toast.makeText(v.getContext(), "Synced", Toast.LENGTH_SHORT).show();
                syncEmployees();
            }
        };



        gridLayout.setOnClickListener(syncEmp);


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            Toast.makeText(getContext(),savedInstanceState.toString(),Toast.LENGTH_LONG).show();
        }
    }

    public  void syncEmployees(){
        ConnectivityManager cm  = (ConnectivityManager)getContext().getSystemService(getContext().CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null){
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE){
                progressBarSubject.setVisibility(View.VISIBLE);
                new GetHttpResponse(getActivity()).execute();
                Toast.makeText(this.getContext(),"Sync Successful",Toast.LENGTH_SHORT).show();
                db.deleteEmployees();
//                animateFab();
            }else{
                Toast.makeText(this.getContext(), "No Internet", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this.getContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    public void searchEmp(String text){
        subjectsList.clear();
        empID.clear();
        empRFID.clear();
        empNames.clear();
        empImages.clear();
        empRid.clear();
        Cursor cursor = db.searchEmployees(text);
        if (cursor.moveToFirst()){
            do{
                Employees employee = new Employees(
                        cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_eID)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_empName)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_RFID)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_dept)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_pos)), cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_image))
                );
                employee.SubjectName = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_empName));
                subjectsList.add(employee);
                empID.add(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_eID)));
                empRFID.add(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_dept)));
                empNames.add(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_empName)));
                empImages.add(Base64.decode(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_image)).getBytes(),Base64.DEFAULT));
                empRid.add(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_RFID)));
//                empImages
            }while(cursor.moveToNext());
        }
        EmployeeSearchAdapter employeeSearchAdapter = new EmployeeSearchAdapter(this.getContext(), subjectsList);
        if(subjectsList.size() == 0){
            txtEmpty.setVisibility(View.VISIBLE);
        }else{
            txtEmpty.setVisibility(View.GONE);
        }
        recEmployees.setAdapter(employeeSearchAdapter);
    }
    public void loadNames(){// load on start of activity
        subjectsList.clear();
        empID.clear();
        empRFID.clear();
        empNames.clear();
        empImages.clear();
        empRid.clear();


        Cursor cursor = db.getEmployees();
        if (cursor.moveToFirst()){
            do{
                Employees employee = new Employees(
                        cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_eID)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_empName)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_RFID)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_dept)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_pos)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_image))
                );
                employee.SubjectName = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_empName));
                subjectsList.add(employee);
                empID.add(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_eID)));
                empRFID.add(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_dept)));
                empNames.add(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_empName)));
                empImages.add(Base64.decode(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_image)).getBytes(),Base64.DEFAULT));
                empRid.add(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_RFID)));
//                empImages
            }while(cursor.moveToNext());
        }
        empAdapter  = new EmployeeAdapter(this.getContext(),subjectsList,getFragmentManager());
        if(subjectsList.size() == 0){
            txtEmpty.setVisibility(View.VISIBLE);
        }else{
            txtEmpty.setVisibility(View.GONE);
        }
        recEmployees.setAdapter(empAdapter);
    }



    private void saveEmployeeToLocalServer(int id,String name,String rfid,String department,String position,String image){
//        db.deleteEmployees();
        db.addEmployees(id,name,rfid,department,position,image);
    }

    public class GetHttpResponse extends AsyncTask<Void, Integer, Integer>
    {
        public Context context;

        String ResultHolder;



        public GetHttpResponse(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            bar = new ProgressDialog(getActivity());
            bar.setCancelable(false);
            bar.setMessage("Syncing...");
            bar.setIndeterminate(true);
            bar.setCanceledOnTouchOutside(false);
            bar.show();
        }


        @Override
        protected Integer doInBackground(Void... arg0)
        {
            int count = 0;
            HttpServicesClass httpServiceObject = new HttpServicesClass(ServerURL);
            try
            {
                httpServiceObject.ExecuteGetRequest();
                Log.d("Response Code: " , String.valueOf(httpServiceObject.getResponseCode()));
                if(httpServiceObject.getResponseCode() == 200)
                {
                    ResultHolder = httpServiceObject.getResponse();

                    if(ResultHolder != null)
                    {
                        JSONArray jsonArray;

                        try {
                            jsonArray = new JSONArray(ResultHolder);

                            JSONObject jsonObject;
                            count = jsonArray.length();
                            for(int i=0; i<jsonArray.length(); i++)
                            {

                                jsonObject = jsonArray.getJSONObject(i);

                                int id              = jsonObject.getInt("employee_id");
                                String rfid         = jsonObject.getString("rfid");
                                String name         = jsonObject.getString("name");
                                String department   = jsonObject.getString("department_name");
                                String position     = jsonObject.getString("position");
                                String image        = jsonObject.getString("image");
                                Log.d("JsonArray",jsonObject.toString());
                                db.addEmployees(id,name,rfid,department,position,image);
                            }
                        }
                        catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else
                {
                    Toast.makeText(context, httpServiceObject.getErrorMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e("Exception Message",e.getMessage());
//                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            return count;
        }

        @Override
        protected void onPostExecute(Integer result)
        {
            bar.dismiss();
            Common.AlertInfoMessage("Sync success","Sync successful with " + String.valueOf(result) + " employees",getActivity());
        }
    }
}
