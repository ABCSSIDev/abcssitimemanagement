package com.resocoder.materialdesign2.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.resocoder.materialdesign2.R;
import com.resocoder.materialdesign2.adapters.AttendanceAdapter;
import com.resocoder.materialdesign2.constructors.Attendance;
import com.resocoder.materialdesign2.helpers.DatabaseHelper;
import com.resocoder.materialdesign2.helpers.NetworkStateChecker;


import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

public class FragmentAttendance extends Fragment {
    public static Bitmap image = null;
    private DatabaseHelper db;
    private List<Attendance> attendanceList = new ArrayList<>();
    public static ArrayList<Integer> empID;
    public static ArrayList<String> empIMAGE;
    //1 means data is synced and 0 means data is not synced
    public static final int NAME_SYNCED_WITH_SERVER = 1;
    Context context;
    FragmentManager fm;
    private NetworkStateChecker networkStateChecker;
    //Broadcast receiver to know the sync status
    public BroadcastReceiver broadcastReceiver;

    //a broadcast to know weather the data is synced or not
    public static final String DATA_SAVED_BROADCAST = "net.simplifiedcoding.datasaved";

    public AttendanceAdapter timelogAdapter;
    private RecyclerView recTimelogs;
    private View vg;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_attendance, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = this.getContext();
        this.vg = view;


        fm = getActivity().getSupportFragmentManager();
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        db = new DatabaseHelper(this.getContext());
        networkStateChecker = new NetworkStateChecker();
        empID = new ArrayList<Integer>();
        empIMAGE = new ArrayList<String>();

        recTimelogs = view.findViewById(R.id.scrollableview);
        timelogAdapter = new AttendanceAdapter(this.getContext(),attendanceList,fm,vg);
        recTimelogs.setAdapter(timelogAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext());
        recTimelogs.setHasFixedSize(true);
        recTimelogs.setLayoutManager(linearLayoutManager);

        loadTimelogs();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                loadTimelogs();

            }
        };
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(DATA_SAVED_BROADCAST));
    }

    public  void loadTimelogs(){
        networkStateChecker.loadNames(context);
        attendanceList.clear();
        empID.clear();
        empIMAGE.clear();
        Cursor cursor = db.getTimelogs();
        if (cursor.moveToFirst()){
            do{
                Attendance t = new Attendance(
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_rfid)),
                        cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_status)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_timeIn)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_time_out)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_empImage)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_lunchOut)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_lunchIn))
                );
                attendanceList.add(t);
                empID.add(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_tID)));
                empIMAGE.add(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_empImage)));
            }while (cursor.moveToNext());
        }
        timelogAdapter = new AttendanceAdapter(context,attendanceList,fm,vg);

        recTimelogs.setAdapter(timelogAdapter);
    }


    public  void refreshList(){
        timelogAdapter.notifyDataSetChanged();
    }

}
