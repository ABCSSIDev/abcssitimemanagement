package com.resocoder.materialdesign2.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.resocoder.materialdesign2.LoginActivity;
import com.resocoder.materialdesign2.MainActivity;
import com.resocoder.materialdesign2.R;
import com.resocoder.materialdesign2.ReportProblemActivity;
import com.resocoder.materialdesign2.UpdateActivity;
import com.resocoder.materialdesign2.adapters.SettingsAdapter;
import com.resocoder.materialdesign2.app_config.AppConfig;
import com.resocoder.materialdesign2.app_config.HttpServicesClass;
import com.resocoder.materialdesign2.constructors.Settings;
import com.resocoder.materialdesign2.helpers.Common;
import com.resocoder.materialdesign2.helpers.ConnectivityReceiver;
import com.resocoder.materialdesign2.helpers.DatabaseHelper;
import com.resocoder.materialdesign2.helpers.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.Nullable;

public  class FragmentSettings extends Fragment{

    SessionManager sessionManager;
    DatabaseHelper databaseHelper;
    private ListView lv1, lv2;
    private SettingsAdapter settingsAdapter,settingsAdapter2;
    String extDir,oldVersion,latestVersion;
    private ImageView imageComany;
    private TextView txtCompany;

    @Override
    public View onCreateView(LayoutInflater layoutInflater , @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        return layoutInflater.inflate(R.layout.fragment_settings,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lv1 = view.findViewById(R.id.first_settings);
        lv2 = view.findViewById(R.id.second_settings);
        imageComany = view.findViewById(R.id.imageCompany);
        txtCompany = view.findViewById(R.id.textCompany);
        setHeader();
        sessionManager = new SessionManager(getActivity());
        databaseHelper = new DatabaseHelper(getActivity());

        String pref1[] = {
                "Image Quality",
                "Data Syncing",
                "Backup Data"
        };
        int icon1[] = {
                R.mipmap.ic_camera,
                R.mipmap.ic_sync,
                R.mipmap.ic_backup
        };
        String desc1[] = {
                "Specify image quality",
                "Sync data over wifi",
                "Backup data manually"
        };
        String pref2[] = {
                "Check Updates",
                "Switch Account",
                "Report a Problem",
                "Logout"
        };
        int icon2[] = {
                R.mipmap.ic_update,
                R.mipmap.ic_account,
                R.mipmap.ic_report,
                R.mipmap.ic_logout
        };
        String desc2[] = {
                "Check for latest updates",
                "",
                "",
                ""
        };


        List<Settings> preference1 = new ArrayList<>();
        List<Settings> preference2 = new ArrayList<>();
        for (int i = 0; i < pref1.length; i++){
            Settings settings = new Settings(pref1[i],icon1[i],desc1[i]);
            preference1.add(settings);
        }
        for (int i = 0; i < pref2.length; i++){
            Settings settings = new Settings(pref2[i],icon2[i],desc2[i]);
            preference2.add(settings);
        }
        settingsAdapter = new SettingsAdapter(getActivity(),preference1);
        settingsAdapter2 = new SettingsAdapter(getActivity(),preference2);
        lv1.setAdapter(settingsAdapter);
        lv2.setAdapter(settingsAdapter2);
        setListViewByHeight(lv1);
        setListViewByHeight(lv2);

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        imageQuality();
                        break;
                    case 1:

                        break;
                    case 2:
                        backupData();
                        break;
                    default:
                        break;
                }
            }
        });
        lv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        checkConnection();
                        break;
                    case 1:
                        switchAccount();
                        break;
                    case 2:
                        reportProblem();
                        break;
                    case 3:
                        logout();
                        break;
                    default:
                        break;
                }
            }
        });
    }

    public void setListViewByHeight(ListView listView){
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null){
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
   }

   public void imageQuality(){
        String imageQ[] = new String[1000];
       CameraManager cameraManager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
       try{
           assert cameraManager != null;
           String cameraId = cameraManager.getCameraIdList()[1];
           CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);
           StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
           assert map != null;
           for (int i = 0; i<map.getOutputSizes(SurfaceTexture.class).length; i++){
               String size = map.getOutputSizes(SurfaceTexture.class)[i].toString();
               imageQ[i] = size;
           }
       }catch (CameraAccessException e){
           e.printStackTrace();
       }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Image Quality")
            .setSingleChoiceItems( imageQ,0,null)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                // Do something useful withe the position of the selected radio button
            }
        }).show();

   }

   public void backupData(){
       File[] extMounts = getActivity().getExternalFilesDirs(null);
       if(extMounts.length < 2 ){
           extDir = "No extMounts";
       }else{
           File sdRoot = extMounts[1];
           if (sdRoot == null){
               extDir = "No external";
           }else{
               extDir = sdRoot.getAbsolutePath()  ;
           }
       }
       String filename = "BackupData.txt";
       String data = resultSet().toString();
       try{
           byte[] sha1hash;
           File myFile =  new File(extDir,filename);
           sha1hash = data.getBytes("UTF-8");
           String base64 = Base64.encodeToString(sha1hash, Base64.DEFAULT);
           FileOutputStream fos = new FileOutputStream(myFile);
           fos.write(base64.getBytes());
           fos.close();
       } catch (FileNotFoundException e) {
           e.printStackTrace();
           Common.AlertInfoMessage("Error","File not found.",getActivity());
       } catch (IOException e) {
           e.printStackTrace();
           Common.AlertInfoMessage("Error","File not saved.",getActivity());
       }finally {
           Common.AlertInfoMessage("Success","File saved in " + extDir + "/" + filename,getActivity());
       }
   }

   public void checkConnection(){
       boolean isConnected = ConnectivityReceiver.isConnected();
       showSnack(isConnected);
   }

   public void switchAccount(){}

   public void reportProblem(){
        Intent intent = new Intent(getActivity(),ReportProblemActivity.class);
        startActivity(intent);
        Objects.requireNonNull(getActivity()).finish();
   }

   public void logout(){
       sessionManager.setLogin(false);
       databaseHelper.deleteUsers();
       databaseHelper.deleteEmployees();

       Intent intent = new Intent(getActivity(),LoginActivity.class);
       startActivity(intent);
   }

    public JSONArray resultSet(){
        Cursor c = databaseHelper.getUnsyncTimelogs();
        JSONArray jsonArray = new JSONArray();
        int column = c.getColumnCount();

        while (c.moveToNext()){
            JSONObject row = new JSONObject();
            for (int index = 0; index < column; index++){
                try {
                    row.put(c.getColumnName(index),c.getString(index));
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            jsonArray.put(row);
        }
        c.close();
        return jsonArray;
    }


    private void showSnack(boolean isConnected) {
        if (isConnected) {
            getVersion();
        }else{
            Common.AlertInfoMessage("No Internet","Try connecting to the internet and try again",getActivity());
        }
    }

    public void getVersion(){
        new VersionUpdate().execute();
    }

    class VersionUpdate extends AsyncTask<String, String, String> {
        String ResultHolder;
        @Override
        protected String doInBackground(String... params) {
            HttpServicesClass httpServiceObject = new HttpServicesClass(AppConfig.URL_CHECK_VERSION);
            try
            {
                httpServiceObject.ExecutePostRequest();

                if(httpServiceObject.getResponseCode() == 200)
                {
                    ResultHolder = httpServiceObject.getResponse();

                    if(ResultHolder != null)
                    {
                        JSONArray jsonArray;

                        try {
                            jsonArray = new JSONArray(ResultHolder);

                            JSONObject jsonObject;


                            jsonObject = jsonArray.getJSONObject(0);

                            return jsonObject.getString("version_name");

                        }
                        catch (JSONException e) {
                            Log.e("json e",e.toString());
                            e.printStackTrace();
                        }
                    }
                }
                else
                {
                    Common.AlertInfoMessage("Error",httpServiceObject.getErrorMessage(),getActivity());
//                    Toast.makeText(context, httpServiceObject.getErrorMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e)
            {
                Log.e("err",e.toString());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result){
            oldVersion = versionName();
            latestVersion = result;
            if (!latestVersion.equals(oldVersion)){
                Intent intent = new Intent(getActivity(),
                        UpdateActivity.class);
                startActivity(intent);
//                Common.AlertInfoMessage("Update found","asdads",getActivity());
            }else{
                Common.AlertInfoMessage("No Updates","You have the latest version of the app.",getActivity());

            }
        }
    }

    public String versionName(){
        String versionName = "";
        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(),0);
            versionName = packageInfo.versionName;
        }catch(PackageManager.NameNotFoundException e){
            e.printStackTrace();
        }
        return versionName;
    }

    public void setHeader(){
        txtCompany.setText(MainActivity.loc);
        int resource;
        switch (MainActivity.loc_id){
            case 1:
                resource = R.mipmap.ic_conmax;
                break;
            case 2:
                resource = R.mipmap.ic_fabcast;
                break;
            case 3:
                resource = R.mipmap.ic_tekonsult;
                break;
            case 4:
                resource = R.mipmap.ic_creodomus;
                break;
            case 5:
                resource = R.mipmap.ic_creofab;
                break;
            case 6:
                resource = R.mipmap.ic_neoventures;
                break;
            case 7:
                resource = R.mipmap.ic_concrete;
                break;
            case  8:
                resource = R.mipmap.ic_castek;
                break;
            case 9:
                resource = R.mipmap.ic_ags;
                break;
            case 10:
                resource = R.mipmap.ic_creoterra;
                break;
            case 11:
                resource = R.mipmap.ic_legaspi;
                break;
            case 12:
                resource = R.mipmap.ic_creomix;
                break;
            default:
                resource = R.mipmap.user;
                break;
        }
        imageComany.setImageResource(resource);
    }

}
