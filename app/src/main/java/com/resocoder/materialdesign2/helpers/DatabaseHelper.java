package com.resocoder.materialdesign2.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "android_api";

    // Login table name
    private static final String TABLE_USER = "user";
    // Employees table name
    private static final String TABLE_EMPLOYEES = "employees";

    // Timelogs table
    private static final String TABLE_TIMELOGS = "timelogs";

    // Login Table Columns names
    private static final String KEY_ID 			= "id";
    private static final String KEY_NAME 		= "name";
    private static final String KEY_EMAIL 		= "email";
    private static final String KEY_UID 		= "uid";
    private static final String KEY_CREATED_AT 	= "created_at";

    // Employees Table Column names
    public static final String KEY_eID 	= "emp_id";
    public static final String KEY_empName = "emp_name";
    public static final String KEY_RFID  	= "rfid";
    public static final String KEY_dept 	= "department";
    public static final String KEY_pos 	= "position";
    public static final String KEY_image = "image";
    private static final String KEY_office = "office";
    private static final String KEY_mobile = "mobile";
    private static final String KEY_address = "address";
    private static final String KEY_type = "emp_type";
    private static final String KEY_site = "site";

    //timelogs table column names
    public static final String KEY_tID      = "id";
    public static final String KEY_empImage = "image";
    public static final String KEY_rfid     = "rfid";
    public static final String KEY_location = "location";
    public static final String KEY_timeIn   = "time_in";
    public static final String KEY_tiGPS  	= "time_in_gps";
    public static final String KEY_time_out = "time_out";
    public static final String KEY_toGPS  	= "time_out_gps";
    public static final String KEY_lunchIn  = "lunch_in";
    public static final String KEY_liGPS  	= "lunch_in_gps";
    public static final String KEY_lunchOut = "lunch_out";
    public static final String KEY_loGPS  	= "lunch_out_gps";
    public static final String KEY_status   = "status";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE," + KEY_UID + " TEXT,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        String CREATE_EMPLOYEE_TABLE = "CREATE TABLE " + TABLE_EMPLOYEES + "("
                + KEY_eID + " INTEGER, "
                + KEY_empName + " TEXT, "
                + KEY_RFID + " TEXT, "
                + KEY_dept + " TEXT, "
                + KEY_pos + " TEXT, "
                + KEY_image + " LONGTEXT, "
                + KEY_office + " TEXT, "
                + KEY_mobile + " TEXT, "
                + KEY_address + " TEXT, "
                + KEY_type + " TEXT, "
                + KEY_site + " TEXT"
                + ")";
        db.execSQL(CREATE_EMPLOYEE_TABLE);

        String CREATE_TIMELOG_TABLE = "CREATE TABLE " + TABLE_TIMELOGS + "("
                + KEY_tID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_empImage + " TEXT,"
                + KEY_rfid + " TEXT, "
                + KEY_location + " INTEGER, "
                + KEY_timeIn + " DATETIME, "
                + KEY_time_out + " DATETIME DEFAULT NULL, "
                + KEY_lunchOut + " DATETIME DEFAULT NULL, "
                + KEY_lunchIn + " DATETIME DEFAULT NULL, "
                + KEY_status + " TINYINT, "
                + KEY_tiGPS + " TEXT, "
                + KEY_toGPS + " TEXT DEFAULT NULL, "
                + KEY_loGPS + " TEXT DEFAULT NULL, "
                + KEY_liGPS + " TEXT DEFAULT NULL"
                + ")";
        db.execSQL(CREATE_TIMELOG_TABLE);
        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EMPLOYEES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIMELOGS);
        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
    public void addUser(String name, String email, String uid, String created_at) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name); // Name
        values.put(KEY_EMAIL, email); // Email
        values.put(KEY_UID, uid); // Email
        values.put(KEY_CREATED_AT, created_at); // Created At

        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }
    /**
     * Storing employee details
     */
    public void addEmployees(int emp_id, String fname,String rfid,String department,String position,String image){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_eID,emp_id);
        values.put(KEY_empName,fname);
        values.put(KEY_RFID,rfid);
        values.put(KEY_dept,department);
        values.put(KEY_pos,position);
        values.put(KEY_image,image);
        long id = db.insert(TABLE_EMPLOYEES,null,values);
        db.close();

        Log.d(TAG,"New Employees Created: " + id );
    }
    /**
     * Storing timelog details
     */
    public void addTimelog(String img, String rfid, int loc, int status, String time, String gps){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_empImage,img);
        values.put(KEY_rfid,rfid);
        values.put(KEY_location,loc);
        values.put(KEY_timeIn,time);
        values.put(KEY_status,status);
        values.put(KEY_tiGPS,gps);
        db.insert(TABLE_TIMELOGS,null,values);
        db.close();
    }
    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("name", cursor.getString(1));
            user.put("email", cursor.getString(2));
            user.put("uid", cursor.getString(3));
            user.put("created_at", cursor.getString(4));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }

    public HashMap<String,String> getEmpDetails(int id){
        HashMap<String,String>employee = new HashMap<String,String>();
        String query = "SELECT * FROM " + TABLE_EMPLOYEES + " WHERE " + KEY_eID + " = " + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query,null);

        c.moveToFirst();
        if (c.getCount() > 0){
            employee.put("empName",c.getString(1));
            employee.put("rfid",c.getString(2));
            employee.put("dept",c.getString(3));
            employee.put("pos",c.getString(4));
            employee.put("image",c.getString(5));
            employee.put("office",c.getString(6));
            employee.put("mobile",c.getString(7));
            employee.put("address",c.getString(8));
            employee.put("type",c.getString(9));
            employee.put("site",c.getString(10));
        }
        c.close();
        db.close();
        return employee;
    }

    public HashMap<String, String> getDetailedTimeDetails(String date, String rfid){
        HashMap<String,String>timelog = new HashMap<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT " +
                KEY_empImage + "," +
                KEY_timeIn  + "," +
                KEY_time_out + "," +
                KEY_lunchOut + "," +
                KEY_lunchIn + "," +
                KEY_tiGPS  + "," +
                KEY_toGPS + "," +
                KEY_loGPS + "," +
                KEY_liGPS +
                " FROM " + TABLE_TIMELOGS +
                " WHERE date(" + KEY_timeIn + ") = '" + date + "'" +
                "and " + KEY_rfid + " = '" + rfid + "'";
        Cursor c = db.rawQuery(query,null);
        c.moveToFirst();

        if (c.getCount() > 0){
            timelog.put("empImage",c.getString(0));
            timelog.put("timeIn",c.getString(1));
            timelog.put("timeOut",c.getString(2));
            timelog.put("lunchOut",c.getString(3));
            timelog.put("lunchIn",c.getString(4));
            timelog.put("tiGPS",c.getString(5));
            timelog.put("toGPS",c.getString(6));
            timelog.put("loGPS",c.getString(7));
            timelog.put("liGPS",c.getString(8));
        }
        c.close();
        db.close();
        return timelog;
    }
    public String getEmpDetails(String rfid){
        String emp = "";
        String query =  "SELECT "+ KEY_empName +" FROM " + TABLE_EMPLOYEES + " WHERE " + KEY_RFID + " = '" + rfid + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query,null);

        if(c.getCount() > 0 ){
            c.moveToFirst();
            emp = c.getString(c.getColumnIndex(KEY_empName));
        }
        c.close();
        db.close();
        return emp;
    }
    /**
     * Select All usersx`
     */
    public Cursor getEmployees(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_EMPLOYEES + " ORDER BY " + KEY_empName + " COLLATE NOCASE";
//		sql += " WHERE " + KEY_empName + " LIKE '"+i+"%'";
        Cursor c = db.rawQuery(sql,null);
        return c;
    }
    public Cursor searchEmployees(String text){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_EMPLOYEES + " WHERE " +
                KEY_RFID + " LIKE '%" + text
                + "%' ORDER BY " + KEY_empName + " COLLATE NOCASE";
//		sql += " WHERE " + KEY_empName + " LIKE '"+i+"%'";
        Cursor c = db.rawQuery(sql,null);
        return c;
    }
    /**
     * Select All timelogs
     */
    public Cursor getTimelogs(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_TIMELOGS + " where "
                + KEY_timeIn + " >= datetime('now', 'localtime', '-15 hour')";
        Cursor c = db.rawQuery(sql,null);
        return c;
    }

    public int getTimeId(String rfid){
        int id = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        String q = "SELECT " + KEY_tID + " FROM " + TABLE_TIMELOGS + " WHERE "
                + KEY_timeIn + " >= datetime('now', 'localtime', '-20 hour') and " + KEY_rfid + " = '" + rfid + "'";
        Cursor c = db.rawQuery(q,null);
        if (c.moveToFirst()){
            do
            {
                id = c.getInt(0);
            } while(c.moveToNext());
        }
        return id;
    }

    public int getTimeInCount(){
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_TIMELOGS + " where date("
                + KEY_timeIn + ") = date('now', 'start of day')";
        Cursor  c = db.rawQuery(query,null);
        if (c.moveToFirst()){
            count = c.getCount();
        }
        return count;
    }

    public int getTimeOutCount(){
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_TIMELOGS + " where date("
                + KEY_timeIn + ") = date('now', 'start of day') and "
                + KEY_time_out + " is not null";
        Cursor  c = db.rawQuery(query,null);
        if (c.moveToFirst()){
            count = c.getCount();
        }
        return count;
    }
    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }
    /**
     * Re-create database delete all employees and create again
     */
    public void deleteEmployees() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_EMPLOYEES,null,null);
        db.close();

        Log.d(TAG,"Deleted all employees user");
    }

    public void deleteTimelogs() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_TIMELOGS,null,null);
        db.close();

        Log.d(TAG,"Deleted all timelogs");
    }

    public Cursor getUnsyncedNames(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_TIMELOGS + " WHERE " + KEY_status + " = 0; " ;
        Cursor c = db.rawQuery(sql,null);
        return c;
    }
    public Cursor getUnsyncTimelogs(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT "
                + KEY_tID + ", "
                + KEY_rfid + ", "
                + KEY_timeIn + ","
                + KEY_empImage + ","
                + KEY_time_out + " FROM "
                + TABLE_TIMELOGS + "; " ;
        Cursor c = db.rawQuery(sql,null);
        return c;
    }
    public boolean updateNameStatus(int id,int status){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_status,status);
        db.update(TABLE_TIMELOGS,contentValues,KEY_tID + " = " +id, null);
        db.close();
        return true;
    }

    public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public boolean checkTimeIn(String rfid){
        boolean checker = false;
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_TIMELOGS + " where date("
                + KEY_timeIn + ") = date('now', 'start of day') and " + KEY_rfid + " = '" + rfid + "'";
        Cursor c = db.rawQuery(query,null);
        if(c.getCount() == 0){
            checker = false;
        }else{
            checker = true;
        }
        return checker;
    }

    public boolean checkForTime(String rfid){
        boolean checker = false;
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_TIMELOGS + " where "
                + KEY_timeIn + " >= datetime('now','localtime', '-15 hour') and " + KEY_rfid + " = '" + rfid + "' and "
                + KEY_lunchOut + " is not null and " + KEY_lunchIn + " is null";
        Cursor c = db.rawQuery(query,null);
        if (c.getCount() == 0){
            checker = false;
        }else{
            checker = true;
        }

        return checker;
    }


    public String getTimeIn(int id){
        String timeIn = "";
        SQLiteDatabase db = this.getReadableDatabase();
        String q = "SELECT " + KEY_timeIn + " FROM " + TABLE_TIMELOGS + " WHERE " + KEY_tID + " = " + id + "";
        Cursor c = db.rawQuery(q,null);
        if (c.moveToFirst()){
            do
            {
                timeIn = c.getString(0);
            } while(c.moveToNext());
        }
        return timeIn;
    }
    public int getLatestId(){
        int id = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        String q = "SELECT " + KEY_tID + " FROM " + TABLE_TIMELOGS;

        Cursor c = db.rawQuery(q,null);
        if (c.moveToFirst()){
            c.moveToLast();
            id = c.getInt(0);
        }
        id += 1;
        return id;
    }

    public boolean updateTimelog(int id,int status,String date,String gps){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_status,status);
        contentValues.put(KEY_time_out,date);
        contentValues.put(KEY_toGPS,gps);
        db.update(TABLE_TIMELOGS,contentValues,KEY_tID + " = " + id,null);
        db.close();
        return true;
    }

    public boolean updateLunchOut(int id, int status, String date,String gps){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_status,status);
        contentValues.put(KEY_lunchOut,date);
        contentValues.put(KEY_loGPS,gps);
        db.update(TABLE_TIMELOGS,contentValues,KEY_tID + " = " + id, null);
        db.close();
        return true;
    }

    public boolean updateLunchIn(int id, int status, String date,String gps){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_status,status);
        contentValues.put(KEY_lunchIn,date);
        contentValues.put(KEY_liGPS,gps);
        db.update(TABLE_TIMELOGS,contentValues,KEY_tID + " = " + id, null);
        db.close();
        return true;
    }

    public boolean checkTimeOut(String rfid){
        boolean checker = false;
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT " + KEY_time_out + " FROM " + TABLE_TIMELOGS + " where "
                + KEY_timeIn + " >= datetime('now', 'localtime', '-15 hour') and " + KEY_rfid + " = '" + rfid +
                "' ORDER BY " + KEY_timeIn + " DESC LIMIT 1";
        Cursor c = db.rawQuery(query,null);
        if (c.moveToFirst()){
            if (c.getString(0) != null && !c.getString(0).isEmpty() && !c.getString(0).equals("null")){
                checker = true;
            }else{
                checker = false;
            }
        }

        return checker;
    }
    public boolean checkLunchOut(String rfid){
        boolean checker = false;
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_TIMELOGS + " where "
                + KEY_timeIn + " >= datetime('now', 'localtime', '-15 hour') and " + KEY_rfid + " = '" + rfid + "' and "
                + KEY_lunchOut + " is not null";
        Cursor c = db.rawQuery(query,null);
        if (c.getCount() == 0){
            checker = false;
        }else{
            checker = true;
        }

        return checker;
    }
    public Cursor getTimelogOfEmployee(String rfid){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM "
                + TABLE_TIMELOGS + " WHERE "
                + KEY_rfid + " = '" + rfid +"'";
        Cursor c = db.rawQuery(query,null);
        return c;
    }
    public boolean checkTimelog(String date,String rfid){
        boolean checker = false;
        SQLiteDatabase db  = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_TIMELOGS +
                " WHERE date(" + KEY_timeIn + ") = '" + date + "'" +
                "and " + KEY_rfid + " = '" + rfid + "'";
        Cursor c = db.rawQuery(query,null);
        if (c.getCount() == 0){
            checker = false;
        }else{
            checker = true;
        }
        return checker;
    }

    public int getEmpId(String rfid){
        int id = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        String q = "SELECT " + KEY_eID + " FROM " + TABLE_EMPLOYEES + " WHERE "
                + KEY_RFID + " = '" + rfid + "'";
        Cursor c = db.rawQuery(q,null);
        if (c.moveToFirst()){
            do
            {
                id = c.getInt(0);
            } while(c.moveToNext());
        }
        return id;
    }


    public int checkEmployeeCount(){
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_EMPLOYEES;
        Cursor c  = db.rawQuery(query,null);
        count = c.getCount();
        return count;
    }

    public boolean checkName(String rfid){
        boolean checker = false;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_EMPLOYEES +
                " WHERE " + KEY_RFID  + " = '" + rfid + "'";
        Cursor c = db.rawQuery(query,null);
        if (c.getCount() == 0){
            checker = false;
        }else{
            checker = true;
        }
        return checker;
    }

    public boolean checkTimeInToday(String rfid){
        boolean checker = false;
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_TIMELOGS + " where "
                + KEY_timeIn + " >= datetime('now', 'localtime', '-15 hour') and " + KEY_rfid + " = '" + rfid + "' and " +
                KEY_time_out + " IS NULL";
        Cursor c = db.rawQuery(query,null);
        if(c.getCount() == 0){
            checker = false;
        }else{
            checker = true;
        }
        return checker;
    }

    public String dateNow(){
        SQLiteDatabase db = this.getReadableDatabase();
        String date = "";
        String query = "SELECT datetime('now','localtime','-15 hour') as date";
        Cursor c = db.rawQuery(query,null);
        if (c.moveToFirst()){
            do
            {
                date = c.getString(0);
            } while(c.moveToNext());
        }
        return date;
    }

    public HashMap<String,String> getEmpDetailsById(String id){
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT "+KEY_empName+","+KEY_dept+","+KEY_pos+","+ KEY_image +" FROM " + TABLE_EMPLOYEES + " WHERE " + KEY_RFID + " = '"+id+"'" ;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("name", cursor.getString(0));
            user.put("department", cursor.getString(1));
            user.put("position", cursor.getString(2));
            user.put("image",cursor.getString(3));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
        }
}
