package com.resocoder.materialdesign2.helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.resocoder.materialdesign2.app_config.AppConfig;
import com.resocoder.materialdesign2.app_config.AppController;
import com.resocoder.materialdesign2.fragments.FragmentAttendance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NetworkStateChecker extends BroadcastReceiver {
    private Context context;
    private  DatabaseHelper db;
    private File myExternalFile;
    private String serial;
    @Override
    public void onReceive(Context context, Intent intent){
        this.context = context;
        db = new DatabaseHelper(context);
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null){
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE){
                Cursor cursor = db.getUnsyncedNames();

                if (cursor.moveToFirst()){

                    do{
                        String timeOut,lunchOut,lunchIn,timeInGps,timeOutgps,lunchOutGps,lunchInGps;
                        timeOut = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_time_out)));
                        lunchOut = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_lunchOut)));
                        lunchIn = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_lunchIn)));
                        timeInGps = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_tiGPS)));
                        timeOutgps = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_toGPS)));
                        lunchOutGps = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_loGPS)));
                        lunchInGps = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_liGPS)));
                        saveName(
                                cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_tID)),
                                cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_rfid)),
                                cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_location)),
                                cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_timeIn)),
                                timeOut,
                                lunchOut,
                                lunchIn,
                                timeInGps,
                                timeOutgps,
                                lunchOutGps,
                                lunchInGps,
                                cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_empImage))
                        );
                    }while(cursor.moveToNext());
                }
            }
        }
    }

//    protected void onProgressUpdate(Integer... progressUpdate){
//        super.onProgressUpdate(progressUpdate);
//
//    }

    public String checkNull(String value){
        if (value != null){
            return value;
        }else{
            return "";
        }
    }

    public void loadNames(Context c){
        this.context = c;
        db = new DatabaseHelper(context);

        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null){
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE){
                Cursor cursor = db.getUnsyncedNames();

                if (cursor.moveToFirst()){
                    do{
                        String timeOut,lunchOut,lunchIn,timeInGps,timeOutgps,lunchOutGps,lunchInGps, image;
                        timeOut = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_time_out)));
                        lunchOut = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_lunchOut)));
                        lunchIn = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_lunchIn)));
                        timeInGps = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_tiGPS)));
                        timeOutgps = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_toGPS)));
                        lunchOutGps = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_loGPS)));
                        lunchInGps = checkNull(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_liGPS)));

                        saveName(
                                cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_tID)),
                                cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_rfid)),
                                cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_location)),
                                cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_timeIn)),
                                timeOut,
                                lunchOut,
                                lunchIn,
                                timeInGps,
                                timeOutgps,
                                lunchOutGps,
                                lunchInGps,
                                cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_empImage))
                        );
                    }while(cursor.moveToNext());
                }
                if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
                    Toast.makeText(context,"Cant save file",Toast.LENGTH_SHORT).show();
                }
                else {
                    String filename = "DataBackup.txt";
                    String filepath = "MyFileStorage";
                    myExternalFile = new File(context.getExternalFilesDir(filepath), filename);
                    writeFile(resultSet().toString());
                }
            }
        }
    }

    private void saveName(final int id, final String rfid, final int loc,
                          final String time, final String time2, final String lunchOut, final String lunchIn, final String timeInloc, final String timeOutloc, final String lunchOutloc, final String lunchInloc, final String img){
        serial = Build.SERIAL;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_SAVE_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (!obj.getBoolean("error")) {
                        db.updateNameStatus(id, FragmentAttendance.NAME_SYNCED_WITH_SERVER);
                        context.sendBroadcast(new Intent(FragmentAttendance.DATA_SAVED_BROADCAST));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context,e.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,"Error"+error.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }){
            protected Map<String,String> getParams() {
                Map<String,String> params = new HashMap<>();
                params.put("name",rfid);
                params.put("time",time);
                params.put("time_out",time2);
                params.put("lunch_out",lunchOut);
                params.put("lunch_in",lunchIn);
                params.put("status","sync_all");
                params.put("location",String.valueOf(loc));
                params.put("id",String.valueOf(db.getEmpId(rfid)));
                params.put("timeIngps",timeInloc);
                params.put("timeOutgps",timeOutloc);
                params.put("lunchOutgps",lunchOutloc);
                params.put("lunchIngps",lunchInloc);
                params.put("image",img);
                params.put("serial",serial);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    public JSONArray resultSet(){
        Cursor c = db.getUnsyncTimelogs();
        JSONArray jsonArray = new JSONArray();
        int column = c.getColumnCount();

        while (c.moveToNext()){
            JSONObject row = new JSONObject();
            for (int index = 0; index < column; index++){
                try {
                    row.put(c.getColumnName(index),c.getString(index));
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            jsonArray.put(row);
        }
        c.close();
        return jsonArray;
    }
    public void writeFile(String data){
        try {
            FileOutputStream fos = new FileOutputStream(myExternalFile);
            fos.write(data.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState);
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(extStorageState);
    }
}
