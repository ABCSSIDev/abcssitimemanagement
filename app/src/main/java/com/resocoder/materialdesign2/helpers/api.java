
package com.resocoder.materialdesign2.helpers;

        import com.resocoder.materialdesign2.constructors.Login;
        import com.resocoder.materialdesign2.constructors.Users;

        import java.util.List;

        import retrofit2.Call;
        import retrofit2.http.Field;
        import retrofit2.http.FormUrlEncoded;
        import retrofit2.http.GET;
        import retrofit2.http.Header;
        import retrofit2.http.POST;
        import retrofit2.http.Path;

public interface api {
    String BASE_URL = "http://staging.abcsystems.com.ph/abc_api/public/api/auth/";
    String BASE_URL2 = "http://staging.abcsystems.com.ph/pgc_api/public/api/auth/";
    String PGC_URL = "https://124.105.45.99/pgc_apis2/public/api/auth/";

    @FormUrlEncoded
    @POST("login")
    Call<Login> loginWithCredentials(
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("users")
    Call<List<Users>> getEmployees(
            @Header("authorization") String AuthKey
    );


}
