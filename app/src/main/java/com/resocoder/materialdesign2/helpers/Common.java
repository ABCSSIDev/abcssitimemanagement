package com.resocoder.materialdesign2.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Common {
    public static void AlertInfoMessage(String title, String message, Context context){
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.hide();
            }
        });
        alertDialog.show();
    }

    public static Retrofit retroBuild(){
        return new Retrofit.Builder()
                .baseUrl(api.BASE_URL)
//                .client(Common.getUnsafeOKHttpClient().build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
