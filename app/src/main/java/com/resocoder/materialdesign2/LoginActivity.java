package com.resocoder.materialdesign2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.resocoder.materialdesign2.app_config.AppConfig;
import com.resocoder.materialdesign2.constructors.Login;
import com.resocoder.materialdesign2.helpers.Common;
import com.resocoder.materialdesign2.helpers.DatabaseHelper;
import com.resocoder.materialdesign2.helpers.SessionManager;
import com.resocoder.materialdesign2.helpers.api;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends Activity {
    private static final String tag = LoginActivity.class.getSimpleName();
    private Button btnLogin;
    private EditText etUsername,etPassword;
    private ProgressDialog progressDialog;
    private SessionManager session;
    private DatabaseHelper db;
    private String username,password;

    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsername = findViewById(R.id.input_username);
        etPassword = findViewById(R.id.input_password);
        btnLogin = findViewById(R.id.btn_login);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        db = new DatabaseHelper(this);
        session = new SessionManager(this);

        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = etUsername.getText().toString().trim();
                String password = etPassword.getText().toString().trim();

                if (!username.isEmpty() && !password.isEmpty()){
                    //execute login task
//                    new LoginTask().execute(username,password);
                    RetroLogin(username,password);
                }else{
                    etUsername.setError("Required Field");
                    etPassword.setError("Required Field");
                    Toast.makeText(LoginActivity.this, "Please Enter the credentials", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onBackPressed(){
        finish();
        moveTaskToBack(true);
    }

    public void alert (String title,String message){
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void RetroLogin(final String username, String password){
        Retrofit retrofit = Common.retroBuild();

        api service = retrofit.create(api.class);

        Call<Login> call = service.loginWithCredentials(username,password);

        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(@NonNull Call<Login> call, @NonNull Response<Login> response) {
                if (response.isSuccessful()){
                    String token = response.body().getToken_type() + " " + response.body().getAccess_token();
                    session.setLogin(true);
                    session.setAccessToken(token);

//                    Toast.makeText(LoginActivity.this,token,Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);

                }else{
                    Toast.makeText(LoginActivity.this , "Error ",Toast.LENGTH_SHORT).show();

                }
                //                String message = response.body().getMessage();
//                Toast.makeText(LoginActivity.this, message , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Toast.makeText(LoginActivity.this,"Login Failed: Unable to establish a good connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
