package com.resocoder.materialdesign2.constructors;

import com.google.gson.annotations.SerializedName;

public class Users {
    @SerializedName("user_id")
    private int user_id;

    @SerializedName("name")
    public String name;

    @SerializedName("contact_number")
    private String contact_number;

    @SerializedName("rfid")
    private String rfid;

    @SerializedName("department")
    private String department;

    @SerializedName("position")
    private String position;

    @SerializedName("image")
    private String image;

    public Users(int userID, String name, String contact, String rfid, String department, String position, String image) {
        this.user_id = userID;
        this.name = name;
        this.contact_number = contact;
        this.rfid = rfid;
        this.department = department;
        this.position = position;
        this.image = image;
    }

    public int getUserID() { return user_id; }
    public String getName() { return name; }
    public String getContact() { return contact_number; }
    public String getRfid() { return rfid; }
    public String getDepartment() { return department; }
    public String getPosition() { return position; }
    public String getImage() { return image; }

}
