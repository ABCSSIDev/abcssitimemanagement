package com.resocoder.materialdesign2.constructors;

public class Attendance {
    private String rfid;
    private int stat;
    private String time,time2,img,lunch1,lunch2;
    private int id;

    public Attendance(String rfid, int status, String time,String time2, String img, String lunch1, String lunch2){
        this.rfid = rfid;
        this.stat = status;
        this.time = time;
        this.time2 = time2;
        this.img = img;
        this.lunch1 = lunch1;
        this.lunch2 = lunch2;
    }

    public String getRfid() {
        return rfid;
    }

    public int getStat() {
        return stat;
    }

    public String getTime() {
        return time;
    }

    public String getTime2() {
        return time2;
    }

    public String getImg() {
        return img;
    }

    public int getId() {
        return id;
    }

    public String getLunch1() {
        return lunch1;
    }

    public String getLunch2() {
        return lunch2;
    }
}
