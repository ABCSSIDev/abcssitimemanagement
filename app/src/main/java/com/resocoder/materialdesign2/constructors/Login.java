package com.resocoder.materialdesign2.constructors;

import android.text.BoringLayout;

import com.google.gson.annotations.SerializedName;

import javax.xml.transform.Result;

public class Login {
    @SerializedName("error")
    private Boolean error;

    @SerializedName("message")
    private String message;

    @SerializedName("access_token")
    private String access_token;

    @SerializedName("token_type")
    private String token_type;


    @SerializedName("expires_at")
    private String expires_at;



    public Login(Boolean error,String message, String access_token,String token_type, int location, String expires_at,String location_name){
        this.error = error;
        this.message = message;
        this.access_token = access_token;
        this.token_type = token_type;
        this.expires_at = expires_at;
    }

    public Boolean getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getAccess_token() {
        return access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public String getExpires_at() {
        return expires_at;
    }

}
