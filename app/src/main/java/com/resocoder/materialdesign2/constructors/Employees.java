package com.resocoder.materialdesign2.constructors;

public class Employees {
    private int empID;
    public String SubjectName;
    private String rfid;
    private String department;
    private String position;
    private String image;

    public Employees(int id, String name, String rfid,String department, String position, String img){
        this.empID = id;
        this.SubjectName = name;
        this.rfid = rfid;
        this.department = department;
        this.position = position;
        this.image = img;
    }

    public int getEmpID() {
        return empID;
    }

    public String getSubjectName(){
        return SubjectName;
    }

    public String getRfid(){
        return rfid;
    }

    public String getDepartment() {
        return department;
    }

    public String getPosition(){
        return position;
    }

    public String getImage(){
        return image;
    }
}
