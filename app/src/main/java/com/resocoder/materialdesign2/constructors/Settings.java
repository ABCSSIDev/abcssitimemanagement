package com.resocoder.materialdesign2.constructors;

public class Settings {

    public String setting_name,setting_description;
    public int icon;

    public Settings(String setting_name,int icon,String desc){
        this.setting_name = setting_name;
        this.icon = icon;
        this.setting_description = desc;
    }


}
