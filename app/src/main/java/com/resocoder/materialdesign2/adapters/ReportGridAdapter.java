package com.resocoder.materialdesign2.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.design.widget.FloatingActionButton;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.resocoder.materialdesign2.R;

import java.util.ArrayList;

public class ReportGridAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> imageUrls;
    private DisplayImageOptions options;

    public ReportGridAdapter(Context context, ArrayList<String> imageUrls, boolean isCustomGalleryActivity) {
        this.context = context;
        this.imageUrls = imageUrls;
        SparseBooleanArray mSparseBooleanArray = new SparseBooleanArray();

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    @Override
    public int getCount() {
        return imageUrls.size();
    }

    @Override
    public Object getItem(int position) {
        return imageUrls.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = layoutInflater.inflate(R.layout.gridview_adapter, parent, false);//Inflate layout

        final ImageView imageView =  convertView.findViewById(R.id.reportImageView);
        final FloatingActionButton removeItem = convertView.findViewById(R.id.removeItem);

        removeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeItem(position);
            }
        });
        ImageLoader.getInstance().displayImage("file://" + imageUrls.get(position), imageView, options);
        return convertView;
    }

    public void removeItem(int position){
        imageUrls.remove(position);

        notifyDataSetChanged();
    }
}
