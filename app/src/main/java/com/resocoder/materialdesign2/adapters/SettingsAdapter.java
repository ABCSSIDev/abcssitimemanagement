package com.resocoder.materialdesign2.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.resocoder.materialdesign2.R;
import com.resocoder.materialdesign2.constructors.Settings;

import java.util.ArrayList;
import java.util.List;

public class SettingsAdapter extends ArrayAdapter<Settings> {

    public SettingsAdapter(Context context, List<Settings> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent){
        Settings settings = getItem(pos);
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_settings, parent, false);
        }
        TextView tvName =  convertView.findViewById(R.id.text_setting_title);
        ImageView ivIcon = convertView.findViewById(R.id.image_settings_icon);
        TextView tvDesc = convertView.findViewById(R.id.text_setting_description);
        assert settings != null;
        tvName.setText(settings.setting_name);
        ivIcon.setImageResource(settings.icon);
        tvDesc.setText(settings.setting_description);
        return convertView;
    }
}
