package com.resocoder.materialdesign2.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.resocoder.materialdesign2.MainActivity;
import com.resocoder.materialdesign2.R;
import com.resocoder.materialdesign2.constructors.Employees;
import com.resocoder.materialdesign2.fragments.BottomSheetFragment;
import com.resocoder.materialdesign2.fragments.FragmentEmployees;
import com.resocoder.materialdesign2.helpers.RoundedImageView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.DessertVh> {
    private List<Employees> employees;

    private Context context;
    private FragmentManager fragmentManager;
    public EmployeeAdapter(Context context,List<Employees> employee,FragmentManager fragmentManager){
        this.context = context;
        this.employees = employee;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public DessertVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_employee, parent, false);
        return new DessertVh(view);
    }

    @Override
    public void onBindViewHolder(final DessertVh holder, final int position) {
        Employees employee = employees.get(position);
//        final String fragmentName = MainActivity.frag;

        holder.empItem.setOnClickListener(new View.OnClickListener() {
            //            final ImageView imageView = holder.empImage;
            @Override
            public void onClick(View v) {
                //Gelo 02-08-2018 line 65-69

                int empid = FragmentEmployees.empID.get(position);
                String rfid = FragmentEmployees.empRFID.get(position);
                String name = FragmentEmployees.empNames.get(position);
                byte[] image = FragmentEmployees.empImages.get(position);
                String rid = FragmentEmployees.empRid.get(position);
                BottomSheetFragment bottomSheetFragment = BottomSheetFragment.newInstance(rid);
                bottomSheetFragment.show(fragmentManager,"add_photo_dialog_fragment");
//                Intent intent = new Intent(context, EmployeeInfo.class);
//                intent.putExtra("empID",empid);
//                intent.putExtra("empName",name);
//                intent.putExtra("empRFID",rfid);
//                intent.putExtra("empImage",image);
//                intent.putExtra("empRid",rid);
//                intent.putExtra("fragmentName",fragmentName);
//                context.startActivity(intent, options.toBundle()); //Gelo 02-08-2018
            }
        });
        holder.mName.setText(employee.getSubjectName());
        holder.mDescription.setText(employee.getDepartment());

        String byteImg = employee.getImage();
        Bitmap bitmap;
        if(byteImg != null && !byteImg.isEmpty() && !byteImg.equals("null")){
            byte[] image = byteImg.getBytes();
            byte[] imageAsBytes = Base64.decode(image,Base64.DEFAULT);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap = RoundedImageView.getRoundedCroppedBitmap(BitmapFactory.decodeByteArray(imageAsBytes,0,imageAsBytes.length),100);
            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
            Bitmap decoded  = BitmapFactory.decodeStream(new ByteArrayInputStream(stream.toByteArray()));
            holder.empImage.setImageBitmap(decoded);

        }else{
            bitmap = RoundedImageView.getRoundedCroppedBitmap(BitmapFactory.decodeResource(context.getResources(),
                    R.mipmap.user),100);
            holder.empImage.setImageBitmap(bitmap);

        }

    }



    @Override
    public int getItemCount() {
        return employees == null ? 0 : employees.size();
    }

    public static class DessertVh extends RecyclerView.ViewHolder {
        private TextView mName;
        private TextView mDescription;
        private ImageView empImage;
        private LinearLayout empItem;
        private final Context c; //Gelo 02-08-2018
        DessertVh(View itemView) {
            super(itemView);
            mName =  itemView.findViewById(R.id.txtRfidTime);
            mDescription =  itemView.findViewById(R.id.txtTimeIn);
            empImage =  itemView.findViewById(R.id.txt_firstletter);
            empItem = itemView.findViewById(R.id.empItem);
            c = itemView.getContext(); //Gelo 02-08-2018

        }
        public void bind(String item){

        }
    }
}
