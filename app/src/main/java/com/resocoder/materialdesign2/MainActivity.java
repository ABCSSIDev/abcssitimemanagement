package com.resocoder.materialdesign2;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.support.design.bottomappbar.BottomAppBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.resocoder.materialdesign2.app_config.AppConfig;
import com.resocoder.materialdesign2.app_config.AppController;
import com.resocoder.materialdesign2.app_config.HttpServicesClass;
import com.resocoder.materialdesign2.constructors.Users;
import com.resocoder.materialdesign2.fragments.FragmentAttendance;
import com.resocoder.materialdesign2.fragments.FragmentEmployees;
import com.resocoder.materialdesign2.fragments.FragmentSettings;
import com.resocoder.materialdesign2.helpers.Common;
import com.resocoder.materialdesign2.helpers.ConnectivityReceiver;
import com.resocoder.materialdesign2.helpers.DatabaseHelper;
import com.resocoder.materialdesign2.helpers.SessionManager;
import com.resocoder.materialdesign2.helpers.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {
    public static int loc_id;
    public static  String loc;
    public FloatingActionButton scan_fab;
    private DatabaseHelper db;
    private SessionManager session;
    private boolean doubleBackToExitPressedOnce = false;
    ProgressDialog bar;
    private String ServerURL = AppConfig.ServerURL;
    public FrameLayout viewConnectionStat;
    public TextView txtConnectionStat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomAppBar bottomAppBar = findViewById(R.id.bottom_app_bar);
        setSupportActionBar(bottomAppBar);

        viewConnectionStat = findViewById(R.id.view2);
        txtConnectionStat = findViewById(R.id.textView2);

        //helpers
//        db = new DatabaseHelper(this);
        session = new SessionManager(this);

//        int i = db.checkEmployeeCount();
//        if (i == 0){
//            showDialogBox();
//        }
        Log.w("session", session.getAccessToken());
        if (!session.isLoggedIn()){
            logoutUser();
        }
//        else{
//            HashMap<String, String> user = db.getUserDetails();
//            loc = user.get("name");
//            loc_id = Integer.parseInt(user.get("uid"));
//            ServerURL = ServerURL + "?loc="+ loc_id;
//        }
        RetroEmployees(session.getAccessToken());

//        changeFragment(new FragmentAttendance());
//        checkConnection();

        scan_fab = findViewById(R.id.scan_rfid);
        scan_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,ScanRfid.class);
                startActivity(intent);
                finish();
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bottom_app_bar_menu, menu);
        return true;
    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem menuItem){
//        switch (menuItem.getItemId()) {
//            case R.id.today_tab:
//                changeFragment(new FragmentAttendance());
//                break;
//            case R.id.employees_tab:
//                changeFragment(new FragmentEmployees());
//                break;
//            default:
//                changeFragment(new FragmentSettings() );
//                break;
//        }
//        return true;
//    }

//    @Override
//    protected void onResume() {
//        super.onResume();
////        shoudExecute = false;
//        // register connection status listener
//        AppController.getInstance().setConnectivityListener(this);
//    }
//    @Override
//    protected void onPause(){
//        super.onPause();
////        shoudExecute = false;
//    }
//    /**
//     * Callback will be triggered when there is change in
//     * network connection
//     */
//    @Override
//    public void onNetworkConnectionChanged(boolean isConnected) {
//        showSnack(isConnected);
//    }
//
//    private void changeFragment(Fragment targetFragment){
//        getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.main_fragment,targetFragment,"fragment")
//                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                .commit();
//    }

    public void logoutUser(){
        session.setLogin(false);
//        db.deleteUsers();
//        db.deleteEmployees();

        Intent intent = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }

//    private void checkConnection() {
//        boolean isConnected = ConnectivityReceiver.isConnected();
//        showSnack(isConnected);
//    }
//
//    public static Animation inFromTopToBottom(long duration){
//        Animation inFromTop = new TranslateAnimation(
//                Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,  0.0f,
//                Animation.RELATIVE_TO_PARENT,  -1.0f, Animation.RELATIVE_TO_PARENT,   0.0f
//        );
//        return inFromTop;
//    }
//
//    private void showSnack(boolean isConnected) {
//        String message;
//        if (isConnected) {
//            message = "Connected";
////            getVersion();
//            txtConnectionStat.setText(message);
//            viewConnectionStat.setBackgroundResource(R.color.connected);
//            viewConnectionStat.setVisibility(View.VISIBLE);
//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    viewConnectionStat.setVisibility(View.GONE);
//                }
//            }, AppConfig.SPLASH_DELAY);
//        } else {
//            message = "Offline Mode";
//            txtConnectionStat.setText(message);
//            viewConnectionStat.setBackgroundResource(R.color.disconnected);
//            viewConnectionStat.setVisibility(View.VISIBLE);
//        }
//
//    }
//
//    @Override
//    public void onBackPressed(){
//        if (doubleBackToExitPressedOnce) {
//            System.exit(0);
//            return;
//        }
//        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
//
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce=false;
//            }
//        }, 2000);
//    }
//
//    public void showDialogBox(){
//        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
//        String title = "No Employees";
//        String message = "Sync now to fetch employee informations!";
//        alertDialog.setTitle(title);
//        alertDialog.setMessage(message);
//        alertDialog.setCancelable(false);
//        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Sync now", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                syncEmployees();
//            }
//        });
//        alertDialog.show();
//    }
//
//    public  void syncEmployees(){
//        ConnectivityManager cm  = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
//        assert cm != null;
//        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//
//        if (activeNetwork != null){
//            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE){
//                new GetHttpResponse(MainActivity.this).execute();
//                db.deleteEmployees();
////                animateFab();
//            }else{
//                Toast.makeText(MainActivity.this, "No Internet", Toast.LENGTH_SHORT).show();
//            }
//        }else{
//            Toast.makeText(MainActivity.this,"No Internet Connection",Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    public class GetHttpResponse extends AsyncTask<Void, Integer, Integer>
//    {
//        public Context context;
//
//        String ResultHolder;
//
//
//
//        public GetHttpResponse(Context context)
//        {
//            this.context = context;
//        }
//
//        @Override
//        protected void onPreExecute()
//        {
//            super.onPreExecute();
//            bar = new ProgressDialog(MainActivity.this);
//            bar.setCancelable(false);
//            bar.setMessage("Syncing...");
//            bar.setIndeterminate(true);
//            bar.setCanceledOnTouchOutside(false);
//            bar.show();
//        }
//
//
//        @Override
//        protected Integer doInBackground(Void... arg0)
//        {
//            int count = 0;
//            HttpServicesClass httpServiceObject = new HttpServicesClass(ServerURL);
//            try
//            {
//                httpServiceObject.ExecuteGetRequest();
//                Log.d("Response Code: " , String.valueOf(httpServiceObject.getResponseCode()));
//                if(httpServiceObject.getResponseCode() == 200)
//                {
//                    ResultHolder = httpServiceObject.getResponse();
//
//                    if(ResultHolder != null)
//                    {
//                        JSONArray jsonArray;
//
//                        try {
//                            jsonArray = new JSONArray(ResultHolder);
//
//                            JSONObject jsonObject;
//                            count = jsonArray.length();
//                            for(int i=0; i<jsonArray.length(); i++)
//                            {
//
//                                jsonObject = jsonArray.getJSONObject(i);
//
//                                int id              = jsonObject.getInt("employee_id");
//                                String rfid         = jsonObject.getString("rfid");
//                                String name         = jsonObject.getString("name");
//                                String department   = jsonObject.getString("department_name");
//                                String position     = jsonObject.getString("position");
//                                String image        = jsonObject.getString("image");
//                                Log.d("JsonArray",jsonObject.toString());
//                                db.addEmployees(id,name,rfid,department,position,image);
//                            }
//                        }
//                        catch (JSONException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//                else
//                {
//                    Toast.makeText(context, httpServiceObject.getErrorMessage(), Toast.LENGTH_SHORT).show();
//                }
//            }
//            catch (Exception e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//                Log.e("Exception Message",e.getMessage());
////                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//            return count;
//        }
//
//        @Override
//        protected void onPostExecute(Integer result)
//        {
//            bar.dismiss();
//            Common.AlertInfoMessage("Sync success","Sync successful with " + String.valueOf(result) + " employees",MainActivity.this);
//        }
//    }


    public void RetroEmployees(String access_token){
        Retrofit retrofit = Common.retroBuild();

        api service = retrofit.create(api.class);

        Call<List<Users>> call = service.getEmployees(access_token);
        call.enqueue(new Callback<List<Users>>() {
            @Override
            public void onResponse(Call<List<Users>> call, Response<List<Users>> response) {
                Log.e("HELLO: ", response.body().toString());
            }

            @Override
            public void onFailure(Call<List<Users>> call, Throwable t) {
                Toast.makeText(MainActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
                Log.e("NO: ", t.getMessage());
            }
        });
    }
}
