package com.resocoder.materialdesign2;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.resocoder.materialdesign2.app_config.AppConfig;
import com.resocoder.materialdesign2.helpers.DatabaseHelper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ScanRfid extends AppCompatActivity {
    private static final String TAG = "AndroidApiCamera";
    private EditText etRfid;
    private TextureView textureView;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0,90);
        ORIENTATIONS.append(Surface.ROTATION_90,0);
        ORIENTATIONS.append(Surface.ROTATION_180,270);
        ORIENTATIONS.append(Surface.ROTATION_270,180);
    }

    protected CameraDevice cameraDevice;
    protected CameraCaptureSession cameraCaptureSessions;
    //    protected CaptureRequest captureRequest;
    protected CaptureRequest.Builder captureRequestBuilder;
    private Size imageDimension;
    private ImageReader imageReader;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    //    private boolean mFlashSupported;
    private android.os.Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    private ProgressDialog progressDialog;
    public double lat;
    public double longitude;
    public String coordinates;
    private String serial,model;
    private DatabaseHelper db;
    private byte[] bytes;
    public static final int NAME_SYNCED_WITH_SERVER = 1;
    public static final int NAME_NOT_SYNCED_WITH_SERVER = 0;

    String pref_cam_res,pref_storage_path;
    Boolean pref_upload_wifi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_rfid);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ScanRfid.this);
        pref_cam_res = settings.getString("key_image_quality",null);
        pref_upload_wifi = settings.getBoolean("key_sync_over_wifi",false);
        pref_storage_path = settings.getString("set_storage_path",null);

        db = new DatabaseHelper(this);
//        GPSTracker gps = new GPSTracker(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        serial = Build.SERIAL;
        model  = Build.MODEL;
//        lat = gps.getLatitude();
//        longitude = gps.getLongitude();
//        coordinates = "" + lat + "," + longitude;

        textureView = findViewById(R.id.textureView);
        assert textureView != null;
        textureView.setSurfaceTextureListener(textureListener);

        etRfid = findViewById(R.id.txtRfid);
        assert etRfid != null;
        etRfid.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN){
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_ENTER:
//                            boolean checkName = db.checkName(etRfid.getText().toString().trim());
                            boolean checker = db.checkTimeInToday(etRfid.getText().toString().trim());
                            if (checker){// if has already time in
                                boolean identifier = db.checkForTime(etRfid.getText().toString().trim());
                                if (identifier){
                                    showFragDialog2();
                                }else{
                                    showFragDialog();
                                }
                            }else{
                                progressDialog.setMessage("Saving Time entry");
                                showDialog();
                                takePicture();
//                                    dispatchTakePictureIntent();
                            }
//

                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed(){
        getBack();
    }

    public void getBack(){
//        MainActivity.shoudExecute = false;
        Intent intent = new Intent(ScanRfid.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
    public void showDialog(){
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    public void hideDialog(){
        if (progressDialog.isShowing())
            progressDialog.hide();
    }

    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };

    private void openCamera(){
        CameraManager cameraManager = (CameraManager)getSystemService(Context.CAMERA_SERVICE);
        Log.e(TAG,"is Camera Open");
        try {
            String cameraId = cameraManager.getCameraIdList()[1];
            CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;

            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            if (pref_cam_res != null){
                imageDimension = Size.parseSize(pref_cam_res);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ScanRfid.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                return;
            }
            cameraManager.openCamera(cameraId,stateCallback,null);
        }catch (CameraAccessException e){
            e.printStackTrace();
        }
        Log.e(TAG, "openCamera X");
    }
    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
    }

    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            Log.e(TAG, "onOpened");
            cameraDevice = camera;
            createCameraPreview();
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            cameraDevice.close();
        }

        @Override
        public void onError(CameraDevice camera,  int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };

    protected void createCameraPreview() {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            Surface surface = new Surface(texture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback(){
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return;
                    }
                    // When the session is ready, we start displaying the preview.
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }
                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(ScanRfid.this, "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
    protected void updatePreview() {
        if(null == cameraDevice) {
            Log.e(TAG, "updatePreview error, return");
        }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    protected void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }
    protected void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        startBackgroundThread();
        if (textureView.isAvailable()) {
            openCamera();
        } else {
            textureView.setSurfaceTextureListener(textureListener);
        }
    }
    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    protected void takePicture() {
        if(null == cameraDevice) {
            Log.e(TAG, "cameraDevice is null");
            return;
        }
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());

            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            int width = 176;
            int height = 144;
            if (pref_cam_res != null){
                Size size = Size.parseSize(pref_cam_res);
                width = size.getWidth();
                height = size.getHeight();
            }
            ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
            List<Surface> outputSurfaces = new ArrayList<Surface>(2);
            outputSurfaces.add(reader.getSurface());
            outputSurfaces.add(new Surface(textureView.getSurfaceTexture()));
            final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            // Orientation
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));
            final File file = new File(Environment.getExternalStorageDirectory()+"/pic.jpg");
            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    Image image = null;
                    try {
                        image = reader.acquireLatestImage();
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                        saveTimelogToServer(bitmap);
                    } finally {
                        if (image != null) {
                            image.close();
                        }
                    }
                }
                //function for time in

            };
            reader.setOnImageAvailableListener(readerListener, mBackgroundHandler);
            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);

                    createCameraPreview();
                }
            };
            cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession session) {
                    try {
                        session.capture(captureBuilder.build(), captureListener, mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onConfigureFailed(CameraCaptureSession session) {
                }
            }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    // convert from bitmap to byte array
    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public void saveTimelogToServer(Bitmap empImage){
        final String imgString = Base64.encodeToString(getBytesFromBitmap(empImage),
                Base64.NO_WRAP);
        final int loc = MainActivity.loc_id;

        final String rfid = etRfid.getText().toString().trim();
        final String curr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        final String sqlite_id = String.valueOf(db.getLatestId());

        Map<String,String> params = new HashMap<String,String>();
        params.put("image",imgString);
        params.put("name",rfid);
        params.put("time", curr);
        params.put("id",String.valueOf(db.getEmpId(rfid)));
        params.put("location",String.valueOf(loc));
        params.put("status","time_in");
        params.put("gps",coordinates);
        params.put("serial",serial);
        params.put("sqlite_id",sqlite_id);
        params.put("model",model);
        params.put("datetime","");
        new SaveTimeEntry().execute(params);
    }

    //save time in to local database
    private void saveTimelogToLocalStorage(String img, String rfid, int stat, String time,String coordinates){
        etRfid.setText("");
        db.addTimelog(img,rfid,MainActivity.loc_id,stat,time,coordinates);
        getBack();
    }

    public void showFragDialog2(){
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("Time In");

        alertDialog.setMessage("Time in from lunch");

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,"Time In",new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id) {
                progressDialog.setMessage("Saving Time entry");
                showDialog();
                updateLunchInFromServer();
            }
        });

//        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,"Button 2",new DialogInterface.OnClickListener(){
//            public void onClick(DialogInterface dialog, int id) {
//                //...
//            }
//        });


        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
//                boolean checkTimeOut = db.checkTimeOut();

//                ((AlertDialog)alertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED);
            }
        });
        alertDialog.show();
    }

    public void showFragDialog(){
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("Time Out");
        boolean checkTimeout = db.checkTimeOut(etRfid.getText().toString().trim());
        String message;
        if (checkTimeout){
            message = "You are already timed out";
        }else{
            message = "Choose an action.";
        }
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,"Time Out",new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id) {
                progressDialog.setMessage("Saving Time entry");
                showDialog();
                updateTimelogToServer();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,"Lunch Out",new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id) {
                progressDialog.setMessage("Saving Time entry");
                showDialog();
                lunchOutToServer();
            }
        });

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                boolean checkTimeout = db.checkTimeOut(etRfid.getText().toString().trim());
                if (checkTimeout){
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setEnabled(false);

                }
                boolean checkLunchOut = db.checkLunchOut(etRfid.getText().toString().trim());
                if (checkLunchOut){
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setEnabled(false);
                }
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED);

            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                etRfid.setText("");
            }
        });

        alertDialog.show();
    }

    private void updateLunchInFromServer(){
        final ProgressDialog pDialog  = new ProgressDialog(this);
        pDialog.setMessage("Saving Time entry");
        pDialog.show();

        String tag_string_req = "req_login";

        final String rfid = etRfid.getText().toString().trim();
        final int id = db.getTimeId(rfid);
        final String curr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("image","");
        params.put("name",rfid);
        params.put("time", curr);
        params.put("id",String.valueOf(db.getEmpId(rfid)));
        params.put("location",String.valueOf(MainActivity.loc_id));
        params.put("status","lunch_in");
        params.put("gps",coordinates);
        params.put("serial",serial);
        params.put("sqlite_id", String.valueOf(id));
        params.put("model",model);
        params.put("datetime",db.getTimeIn(id));

        new SaveTimeEntry().execute(params);
    }

    //function for saving lunch in to local storage
    public void updateLunchInToLocalStorage(int id,int stat, String date,String coordinates){
        etRfid.setText("");
        db.updateLunchIn(id,stat,date,coordinates);
        getBack();
    }

    private void updateTimelogToServer(){

        final String rfid = etRfid.getText().toString().trim();
        final int id = db.getTimeId(rfid);
        final String curr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault()).format(new Date());

        Map<String, String> params = new HashMap<String, String>();
        params.put("image","");
        params.put("name",rfid);
        params.put("time", curr);
        params.put("id",String.valueOf(db.getEmpId(rfid)));
        params.put("location",String.valueOf(MainActivity.loc_id));
        params.put("status","time_out");
        params.put("gps",coordinates);
        params.put("serial",serial);
        params.put("sqlite_id", String.valueOf(id));
        params.put("model",model);
        params.put("datetime",db.getTimeIn(id));

        new SaveTimeEntry().execute(params);

    }

    //function for saving time out to local database
    public void updateTimelogToLocalStorage(int id,int stat, String date,String coordinates){
        etRfid.setText("");
        db.updateTimelog(id,stat,date,coordinates);
        getBack();
    }

    //function for lunch out
    private void lunchOutToServer(){
        final ProgressDialog pDialog  = new ProgressDialog(this);
        pDialog.setMessage("Saving Time entry");
        pDialog.show();

        final String rfid = etRfid.getText().toString().trim();
        final int id = db.getTimeId(rfid);
        final String curr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        // Posting parameters t2eo login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("image","");
        params.put("name",rfid);
        params.put("time", curr);
        params.put("id",String.valueOf(db.getEmpId(rfid)));
        params.put("location",String.valueOf(MainActivity.loc_id));
        params.put("status","lunch_out");
        params.put("gps",coordinates);
        params.put("serial",serial);
        params.put("sqlite_id", String.valueOf(id));
        params.put("model",model);
        params.put("datetime",db.getTimeIn(id));

        new SaveTimeEntry().execute(params);
    }

    //function for saving lunchout to local database
    public void updateLunchOutToLocalStorage(int id,int stat, String date,String coordinates){
        etRfid.setText("");
        db.updateLunchOut(id,stat,date,coordinates);
        getBack();
    }


    private class SaveTimeEntry extends AsyncTask<Map<String,String>,Boolean,Map<String,String>> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected Map<String, String> doInBackground(Map<String,String>... params) {
            Map<String,String> parameters = new HashMap<>();
            String status       = params[0].get("status");//time in,out,etc
            String image        = params[0].get("image");//for the captured image
            String name         = params[0].get("name");//for the rfid
            String time         = params[0].get("time");//time entry
            String id           = params[0].get("id");
            String location     = params[0].get("location");
            String gps          = params[0].get("gps");
            String serial       = params[0].get("serial");
            String model        = params[0].get("model");
            String sqlite_id    = params[0].get("sqlite_id");
            String datetime     = params[0].get("datetime");

            parameters.put("status",status);
            parameters.put("image",image);
            parameters.put("name",name);
            parameters.put("time",time);
            parameters.put("gps",gps);
            parameters.put("sqlite_id",sqlite_id);
            try{
                url = new URL(AppConfig.URL_SAVE_NAME);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            try{
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(AppConfig.READ_TIMEOUT);
                conn.setConnectTimeout(AppConfig.CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                conn.setDoInput(true);
                conn.setDoOutput(true);

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("status", status)
                        .appendQueryParameter("image", image)
                        .appendQueryParameter("name",name)
                        .appendQueryParameter("time",time)
                        .appendQueryParameter("id",id)
                        .appendQueryParameter("location",location)
                        .appendQueryParameter("gps",gps)
                        .appendQueryParameter("serial",serial)
                        .appendQueryParameter("model",model)
                        .appendQueryParameter("datetime",datetime);
                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            }  catch (IOException e) {
                e.printStackTrace();
            }
            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {
                    // Pass data to onPostExecute method
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
//                    return(result.toString());

                    parameters.put("stat", String.valueOf(NAME_SYNCED_WITH_SERVER));
                    parameters.put("result", result.toString());
                    return parameters;

                }else{
                    parameters.put("stat", String.valueOf(NAME_NOT_SYNCED_WITH_SERVER));
                    return parameters;
                }

            } catch (IOException e) {
                e.printStackTrace();
                parameters.put("stat", String.valueOf(NAME_NOT_SYNCED_WITH_SERVER));
                return parameters;
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(Map<String,String> result){
            hideDialog();
            String status   = result.get("status");
            String image    = result.get("image");
            String name     = result.get("name");
            String time     = result.get("time");
            String gps      = result.get("gps");
            String stat     = result.get("stat");
            String id       = result.get("sqlite_id");

//            String res = result.get("result");
//
//            Log.d("result",res);

            if(status.equalsIgnoreCase("time_in")){
                saveTimelogToLocalStorage(image , name , Integer.parseInt(stat),time , gps);
            }else if(status.equalsIgnoreCase("lunch_out")){
                updateLunchOutToLocalStorage(Integer.parseInt(id),Integer.parseInt(stat),time,gps);
            }else if(status.equalsIgnoreCase("lunch_in")){
                updateLunchInToLocalStorage(Integer.parseInt(id),Integer.parseInt(stat),time,gps);
            }else if( status.equalsIgnoreCase("time_out") ){
                updateTimelogToLocalStorage(Integer.parseInt(id),Integer.parseInt(stat),time,gps);
            }
        }

    }
}
